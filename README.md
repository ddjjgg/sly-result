# SlyResult

**(UNDER CONSTRUCTION - CURRENTLY UNSTABLE)**

SlyResult is a (very) simple library that provides some useful data-types for
representing whether or not a function has failed, as well as the reason behind
the failure.

In a way, the data-types and values that SlyResult defines are similar to
\#errno values, in that a set of common causes of failure are represented by
constants. Unlike \#errno values, however, a semantic distinction is made
between "stochastic results" and "deterministic results".

A function has "stochastic results" if it can fail even if the arguments to the
function are perfectly valid. Common examples of functions with stochastic
results would be functions that call malloc (), use file I/O operations, or
perform network access. In all of these cases, the function call can fail no
matter what the caller does (examples given respectively : a separate process
uses up all available memory; a volume gets disconnected mid-file-operation; a
network link goes down mid-transmission)

On the other hand, a function has "deterministic results" if it can be
guaranteed to succeed by having properly formatted arguments. Examples of
functions with deterministic results would be raw math operations (addition,
multiplication, etc.), simple getter/setter functions, or function that only
call other functions with deterministic results.

To indicate a function has stochastic results, the type \#SlySr can be used as
a return value. To indicate a function has deterministic results, the type
\#SlyDr can be used. With this in mind, when a function returns \#SlySr, we
know that its results must always be checked. When a function returns \#SlyDr,
the function (ideally) only needs to have its results checked for debugging
purposes.
