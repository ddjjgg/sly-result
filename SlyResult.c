/*!****************************************************************************
 *
 * @file
 * SlyResult.c
 *
 * Implements the functions that produce #SlyStdResStr's. #SlySrStr's, and
 * #SlyDrStr's as described in SlyResult.h. These functions are useful for
 * printing out result values in a readable format.
 *
 *****************************************************************************/




#include <string.h>




#include "./SlyResult.h"




/*! 
 * Prints out the value contained by a #SLY_STD_RES enumerated value.
 *
 *  @param s			The #SLY_STD_RES to convert to a #SlyStdResStr
 *  				containing its representation as a string
 *
 *  @return			A #SlyStdResStr containing the string
 *				representation of #s
 *
 *  EXAMPLE
 *
 *  @code
 *
 *	SLY_STD_RES res = SLY_SUCCESS;
 *
 *	printf
 *	(
 *		"Here is a string representing a #SLY_STD_RES value : %s\n",
 *		SlyStdResStr_get (res).str
 *	);
 *
 *  @endcode
 */

SlyStdResStr SlyStdResStr_get (SLY_STD_RES s)
{
	SlyStdResStr sstr;

	if (SLY_SUCCESS == s)
	{
		strncpy (sstr.str, "SLY_SUCCESS", LEN_SlyStdResStr);
	}

	else if (SLY_BAD_ALLOC == s)
	{
		strncpy (sstr.str, "SLY_BAD_ALLOC", LEN_SlyStdResStr);
	}

	else if (SLY_BAD_ARG == s)
	{
		strncpy (sstr.str, "SLY_BAD_ARG", LEN_SlyStdResStr);
	}

	else if (SLY_BAD_CALL == s)
	{
		strncpy (sstr.str, "SLY_BAD_CALL", LEN_SlyStdResStr);
	}

	else if (SLY_BAD_LIB_CALL == s)
	{
		strncpy (sstr.str, "SLY_BAD_LIB_CALL", LEN_SlyStdResStr);
	}

	else if (SLY_BAD_TIMING == s)
	{
		strncpy (sstr.str, "SLY_BAD_TIMING", LEN_SlyStdResStr);
	}

	else if (SLY_NO_CONSEQ == s)
	{
		strncpy (sstr.str, "SLY_NO_CONSEQ", LEN_SlyStdResStr);
	}

	else if (SLY_NO_IMPL == s)
	{
		strncpy (sstr.str, "SLY_NO_IMPL", LEN_SlyStdResStr);
	}

	else if (SLY_NO_WORK == s)
	{
		strncpy (sstr.str, "SLY_NO_WORK", LEN_SlyStdResStr);
	}

	else if (SLY_UNKNOWN_ERROR == s)
	{
		strncpy (sstr.str, "SLY_UNKNOWN_ERROR", LEN_SlyStdResStr);
	}

	else
	{
		strncpy (sstr.str, "(??? SLY_STD_RES)", LEN_SlyStdResStr);
	}


	//Ensure that the last character is always NULL

	sstr.str [LEN_SlyStdResStr - 1] = '\0';


	return sstr;
}




/*! Prints out the value contained by a #SlySr
 *
 *  @param s			The #SlySr to convert to a #SlySrStr
 *  				containing its representation as a string
 *
 *  @return			A #SlySrStr containing the string
 *				representation of #s
 */

SlySrStr SlySrStr_get (SlySr s)
{
	//Get the #SlyStdResStr representing #s.res, and then use it to fill
	//the value of a #SlySrStr

	SlyStdResStr stds = SlyStdResStr_get (s.res);


	SlySrStr sr_str;

	for (size_t char_sel = 0; char_sel < LEN_SlySrStr; char_sel += 1)
	{
		if (char_sel < LEN_SlyStdResStr)
		{
			sr_str.str [char_sel] = stds.str [char_sel];
		}

		else
		{
			sr_str.str [char_sel] = '\0';
		}
	}


	//Ensure that the last character in #sr_str.str is a NULL terminator

	sr_str.str [LEN_SlySrStr - 1] = '\0';


	return sr_str;
}




/*! Prints out the value contained by a #SlyDr enumerated value.
 *
 *  @param s			The #SlyDr to convert to a #SlyDrStr
 *  				containing its representation as a string
 *
 *  @return			A #SlyDrStr containing the string
 *				representation of #s
 */

SlyDrStr SlyDrStr_get (SlyDr s)
{
	//Get the #SlyStdResStr representing #s.res, and then use it to fill
	//the value of a #SlyDrStr

	SlyStdResStr stds = SlyStdResStr_get (s.res);


	SlyDrStr dr_str;

	for (size_t char_sel = 0; char_sel < LEN_SlyDrStr; char_sel += 1)
	{
		if (char_sel < LEN_SlyStdResStr)
		{
			dr_str.str [char_sel] = stds.str [char_sel];
		}

		else
		{
			dr_str.str [char_sel] = '\0';
		}
	}


	//Ensure that the last character in #dr_str.str is a NULL terminator

	dr_str.str [LEN_SlyDrStr - 1] = '\0';


	return dr_str;
}

