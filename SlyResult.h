/*!****************************************************************************
 *
 * @file
 * SlyResult.h
 *
 * Contains data types that are useful for communicating a function's success
 * to other functions in a standard manner.
 *
 * In a way, the types provided here are similar to #errno values. The most
 * notable difference, however, is that a distinction is made between
 * "Stochastic Results" and "Deterministic Results".
 *
 * A "Stochastic Result" from a function is one that can NOT be guaranteed to
 * succeed from the function arguments alone. Ergo, a "Stochastic Result" from
 * a function should always be checked for success to be safe. Examples of
 * functions with stochastic results would be functions that use malloc (),
 * ones that open/close files, or ones that call any functions with
 * stochastic results themselves.
 *
 * A "Deterministic Result" from a function is one that CAN always be
 * guaranteed to succeed provided that the arguments to the function are
 * formatted correctly. As a consequence, the results from these functions can
 * safely be ignored, but can be useful to check when the program is in a
 * debugging mode. Good examples of functions with deterministic results would
 * be functions that perform raw math operations (addition, multiplication,
 * etc.), ones that act as simple setters/getters for an object, or ones that
 * only call other functions with deterministic results.
 *
 * This has some implications for a how a public interface should be
 * interpreted. When a function has stochastic results, that means the caller
 * is expected to always check for that function's success. When a function has
 * deterministic results, the function should work in such a fashion that it
 * absolves the caller from checking for success.
 *
 * If it is ever unclear whether or not a function should have stochastic
 * results or if it should have deterministic results, it is better to err on
 * the side of stochastic results. In the event that a stochastic function acts
 * in a deterministic fashion, it appears to the caller as very reliable, and
 * the branches on failure rarely get stressed. If a deterministic function
 * acts in a stochastic manner, then the function can fail and the caller can
 * not be expected to detect this.
 *
 *****************************************************************************/




#ifndef SLY_RESULT_H
#define SLY_RESULT_H




/// The major version of the current SlyResult header

#define SLY_RESULT_MAJOR_VERSION		(0)


/// The minor version of the current SlyResult header

#define SLY_RESULT_MINOR_VERSION		(3)


/// The patch version of the current SlyResult header

#define SLY_RESULT_PATCH_VERSION		(0)


/// The semantic version of the current SlyResult header

#define SLY_RESULT_SEM_VERSION			"0.3.0"




/*!
 *  Represents common reasons for returning from a function.
 *
 *  There should ONLY be one "SLY_SUCCESS" value, and NO generic "SLY_FAILURE"
 *  value. That way other functions can test "if (SLY_SUCCESS != result)"
 *  correctly, while the other return values can explain why the function
 *  failed.
 *
 *  While this could be used as the return value for a function, it is
 *  probably better to use #SlySr or #SlyDr to indicate whether the
 *  result is stochastic or deterministic.
 */

typedef enum SLY_STD_RES
{

	/// Used to indicate a function successfully executed

	SLY_SUCCESS = 0,


	/// Used to indicate a memory allocation failed, stopping the function
	/// prematurely

	SLY_BAD_ALLOC,


	/// Used to indicate a function was given bad arguments

	SLY_BAD_ARG,


	/// Denotes that calling a function internal to a project has failed
	///
	/// Note, this other failed function might not necessarily be returning
	/// a #SLY_STD_RES itself, hence the existence of this value.
	/// Otherwise, it would usually be better to propagate the
	/// #SLY_STD_RES.

	SLY_BAD_CALL,


	/// Denotes that calling an external library function failed

	SLY_BAD_LIB_CALL,


	/// Denotes that the function failed due to being called at an
	/// unfortunate point in time

	SLY_BAD_TIMING,


	/// Used to indicate that the function had no side-effects for whatever
	/// reason.

	SLY_NO_CONSEQ,


	/// Used to indicate that the function failed because the requested
	/// functionality has yet to be implemented.

	SLY_NO_IMPL,


	/// Used to indicate that there is no work to be done. Similar to a
	/// #SLY_NO_CONSEQ value, but meant to imply that the reason is
	/// specfically the set of work becoming empty.

	SLY_NO_WORK,


	/// Used to indicate a failure that occurred for an unknown reason

	SLY_UNKNOWN_ERROR


} SLY_STD_RES;




/*!
 *  A data type used to represent a stochastic result from a function.
 *
 *  Note, #SlySr and #SlyDr are implemented as struct's so that C++
 *  template deduction can tell the difference between the 2 types.
 */

typedef struct SlySr
{
	/// The #SLY_STD_RES value of the stochastic result

	SLY_STD_RES res;

} SlySr;




/*!
 *  A data type used to represent a deterministic result from a function.
 */

typedef struct SlyDr
{
	/// The #SLY_STD_RES value of the deterministic result

	SLY_STD_RES res;

} SlyDr;




/*!
 *  Determines the length of the string in the #SlyStdResStr struct.
 *
 *  Note, #LEN_SlyStdResStr is separate from #LEN_SlySrStr and
 *  #LEN_SlyDrStr so that their values can be modified easier if the need
 *  to do so ever arises (e.g. a customized version of this header for some
 *  specific project)
 */

#define LEN_SlyStdResStr	(64)




/*!
 *  Determines the length of the string in the #SlySrStr struct.
 */

#define LEN_SlySrStr		(64)




/*!
 *  Determines the length of the string in the #SlyDrStr struct.
 */

#define LEN_SlyDrStr		(64)




/*!
 *  A type that holds a string representing a #SLY_STD_RES value
 */

typedef struct SlyStdResStr
{

	/// A string representing a value of #SLY_STD_RES

	char str [LEN_SlyStdResStr];


} SlyStdResStr;




/*!
 *  A type that holds a string representing a #SlySr value
 */

typedef struct SlySrStr
{

	/// A string representing a value of #SlySr

	char str [LEN_SlySrStr];


} SlySrStr;




/*!
 *  A type that holds a string representing a #SlyDr value
 */

typedef struct SlyDrStr
{

	/// A string representing a value of #SlyDr

	char str [LEN_SlyDrStr];


} SlyDrStr;




/*!
 *  Generates a #SlySr structure from a #SLY_STD_RES value.
 *
 *  The purpose of this function is that it allows you to write...
 *
 *  @code
 *
 *	SlySr some_func ()
 *	{
 *		//...
 *
 *  		return SlySr_get (SLY_SUCCESS);
 *  	}
 *
 *  @endcode
 *
 *  ...instead of
 *
 *  @code
 *
 *	SlySr some_func ()
 *	{
 *		//...
 *
 *		SlySr sr = {.res = SLY_SUCCESS};
 *
 *  		return sr;
 *  	}
 *
 *  @endcode
 *
 *  Keep in mind, the reason #SlySr is implemented as a struct is so that it
 *  is treated distinctly from #SlyDr in C++ overloading.
 *
 *  @param res			The #SLY_STD_RES to convert to a #SlySr
 *
 *  @return			A #SlySr in which #SlySr.res is equivalent to
 *				#SLY_STD_RES
 */

static inline SlySr SlySr_get (SLY_STD_RES res)
{
	SlySr sr = {.res = res};

	return sr;
}




/*!
 *  Generates a #SlyDr structure from a #SLY_STD_RES value.
 *
 *  The purpose of this function is that it allows you to write...
 *
 *  @code
 *
 *	SlyDr some_func ()
 *	{
 *		//...
 *
 *  		return SlyDr_get (SLY_SUCCESS);
 *  	}
 *
 *  @endcode
 *
 *  ...instead of
 *
 *  @code
 *
 *	SlyDr some_func ()
 *	{
 *		//...
 *
 *		SlyDr dr = {.res = SLY_SUCCESS};
 *
 *  		return dr;
 *  	}
 *
 *  @endcode
 *
 *  Keep in mind, the reason #SlyDr is implemented as a struct is so that it
 *  is treated distinctly from #SlySr in C++ overloading.
 *
 *  @param res			The #SLY_STD_RES to convert to a #SlyDr
 *
 *  @return			A #SlyDr in which #SlyDr.res is equivalent to
 *				#SLY_STD_RES
 */

static inline SlyDr SlyDr_get (SLY_STD_RES res)
{
	SlyDr dr = {.res = res};

	return dr;
}




/******************************************************************************
 *
 * Refer to SlyResult.c for information on the following items
 *
 *****************************************************************************/




SlyStdResStr SlyStdResStr_get (SLY_STD_RES res);




SlySrStr SlySrStr_get (SlySr res);




SlyDrStr SlyDrStr_get (SlyDr res);




#endif //SLY_RESULT_H

