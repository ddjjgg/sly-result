#######################
# Compilation Options #
#######################


#Main project options

CC?=			gcc

SRC_TOP_HEADER=		"SlyResult.h"

PROJ_NAME=		SlyResult

HEADER_DIR=		/usr/include

LIBRARY_DIR=		/usr/lib


#This extracts the version number from the main project header

MAJOR_VERSION_GREP=	grep SLY_RESULT_MAJOR_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1
MINOR_VERSION_GREP=	grep SLY_RESULT_MINOR_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1
PATCH_VERSION_GREP=	grep SLY_RESULT_PATCH_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1

MAJOR_VERSION=		$(shell $(MAJOR_VERSION_GREP))
MINOR_VERSION=		$(shell $(MINOR_VERSION_GREP))
PATCH_VERSION=		$(shell $(PATCH_VERSION_GREP))


#Determmine the semantic version when the minor version and patch version are
#masked out

SEM_1_VERSION=		$(MAJOR_VERSION)-x-x
SEM_2_VERSION=		$(MAJOR_VERSION)-$(MINOR_VERSION)-x
SEM_3_VERSION=		$(MAJOR_VERSION)-$(MINOR_VERSION)-$(PATCH_VERSION)

MIN_SEM_1_VERSION=	$(MAJOR_VERSION)
MIN_SEM_2_VERSION=	$(MAJOR_VERSION)-$(MINOR_VERSION)
MIN_SEM_3_VERSION=	$(MAJOR_VERSION)-$(MINOR_VERSION)-$(PATCH_VERSION)


#Determines all of the possible locations that the header/library files and
#links can be installed, based on semantic versions with parts masked out.
#
#During installation, symbolic links to $HEADER_3_DEST will be placed at
#$HEADER_1_DEST and $HEADER_2_DEST. A similar operation will be done for 
#$LIBRARY_3_DEST with LIBRARY_2_DEST and LIBRARY_1_DEST.

HEADER_1=		$(PROJ_NAME)-$(SEM_1_VERSION).h
HEADER_2=		$(PROJ_NAME)-$(SEM_2_VERSION).h
HEADER_3=		$(PROJ_NAME)-$(SEM_3_VERSION).h

LIBRARY_1=		lib$(PROJ_NAME)-$(SEM_1_VERSION).so
LIBRARY_2=		lib$(PROJ_NAME)-$(SEM_2_VERSION).so
LIBRARY_3=		lib$(PROJ_NAME)-$(SEM_3_VERSION).so

HEADER_1_DEST=		$(HEADER_DIR)/$(HEADER_1)
HEADER_2_DEST=		$(HEADER_DIR)/$(HEADER_2)
HEADER_3_DEST=		$(HEADER_DIR)/$(HEADER_3)

LIBRARY_1_DEST=		$(LIBRARY_DIR)/$(LIBRARY_1)
LIBRARY_2_DEST=		$(LIBRARY_DIR)/$(LIBRARY_2)
LIBRARY_3_DEST=		$(LIBRARY_DIR)/$(LIBRARY_3)


#Commands that can be used to find the most recent version of the header/library
#that are installed on the system that match the version at some level of 
#specificity

FIND_HEADER_1_MATCH=	ls -rxv1 $(HEADER_DIR) | grep $(PROJ_NAME)-$(MIN_SEM_1_VERSION) | cut -d$$'\n' -f1
FIND_HEADER_2_MATCH=	ls -rxv1 $(HEADER_DIR) | grep $(PROJ_NAME)-$(MIN_SEM_2_VERSION) | cut -d$$'\n' -f1

FIND_LIBRARY_1_MATCH=	ls -rxv1 $(LIBRARY_DIR) | grep lib$(PROJ_NAME)-$(MIN_SEM_1_VERSION) | cut -d$$'\n' -f1
FIND_LIBRARY_2_MATCH=	ls -rxv1 $(LIBRARY_DIR) | grep lib$(PROJ_NAME)-$(MIN_SEM_2_VERSION) | cut -d$$'\n' -f1 


#Determines the options that will be used to compile the actual library

LIB_CFLAGS=		-Wall -std=gnu11 -g -fPIC -rdynamic -O2
LIB_LDFLAGS=

LIB_SOURCES=		$(wildcard [!test]*.c)\
			$(wildcard [!test]*.h)

LIB_OBJECTS=		$(patsubst %.c,%.o,$(wildcard [!test]*.c))

LIB_DEPENDS=		$(patsubst %,.%.lib_dep,$(LIB_SOURCES))


#Determines the options that will be used to compile the test benches 

TEST_CFLAGS=		-Wall -std=gnu11 -g -fPIC -rdynamic -O2
TEST_LDFLAGS=		-L./ -l$(PROJ_NAME)-$(SEM_3_VERSION)

TEST_SOURCES=		$(wildcard test*.c)\
			$(wildcard test*.h)

TEST_OBJECTS=		$(patsubst %.c,%.o,$(wildcard test*.c))

TEST_DEPENDS=		$(patsubst %,.%.test_dep,$(TEST_SOURCES))




######################
# Debugging Messages #
######################

#$(info $$SEM_1_VERSION			= [${SEM_1_VERSION}])
#$(info $$SEM_2_VERSION			= [${SEM_2_VERSION}])
#$(info $$SEM_3_VERSION			= [${SEM_3_VERSION}])
#$(info $$HEADER_1			= [${HEADER_1}])
#$(info $$HEADER_2			= [${HEADER_2}])
#$(info $$HEADER_3			= [${HEADER_3}])
#$(info $$LIBRARY_1			= [${LIBRARY_1}])
#$(info $$LIBRARY_2			= [${LIBRARY_2}])
#$(info $$LIBRARY_3			= [${LIBRARY_3}])
#$(info $$HEADER_1_DEST			= [${HEADER_1_DEST}])
#$(info $$HEADER_2_DEST			= [${HEADER_2_DEST}])
#$(info $$HEADER_3_DEST			= [${HEADER_3_DEST}])
#$(info $$LIBRARY_1_DEST		= [${LIBRARY_1_DEST}])
#$(info $$LIBRARY_2_DEST		= [${LIBRARY_2_DEST}])
#$(info $$LIBRARY_3_DEST		= [${LIBRARY_3_DEST}])
#$(info $$LIB_CFLAGS			= [${LIB_CFLAGS}])
#$(info $$LIB_LDFLAGS			= [${LIB_LDFLAGS}])
#$(info $$LIB_SOURCES			= [${LIB_SOURCES}])
#$(info $$LIB_OBJECTS			= [${LIB_OBJECTS}])
#$(info $$LIB_DEPENDS			= [${LIB_DEPENDS}])
#$(info $$TEST_CFLAGS			= [${TEST_CFLAGS}])
#$(info $$TEST_LDFLAGS			= [${TEST_LDFLAGS}])
#$(info $$TEST_SOURCES			= [${TEST_SOURCES}])
#$(info $$TEST_OBJECTS			= [${TEST_OBJECTS}])
#$(info $$TEST_DEPENDS			= [${TEST_DEPENDS}])
#$(info $$FIND_HEADER_1_MATCH		= [$(FIND_HEADER_1_MATCH)])
#$(info $$FIND_HEADER_2_MATCH		= [$(FIND_HEADER_2_MATCH)])
#$(info $$FIND_LIBRARY_1_MATCH		= [$(FIND_LIBRARY_1_MATCH)])
#$(info $$FIND_LIBRARY_2_MATCH		= [$(FIND_LIBRARY_2_MATCH)])
#$(info shell $$FIND_HEADER_1_MATCH	= [$(shell $(FIND_HEADER_1_MATCH))])
#$(info shell $$FIND_HEADER_2_MATCH	= [$(shell $(FIND_HEADER_2_MATCH))])
#$(info shell $$FIND_LIBRARY_1_MATCH	= [$(shell $(FIND_LIBRARY_1_MATCH))])
#$(info shell $$FIND_LIBRARY_2_MATCH	= [$(shell $(FIND_LIBRARY_2_MATCH))])




#######################
# Compilation Targets #
#######################

#Used to make the default action the compilation of the library

.PHONY: all
all: $(LIB_OBJECTS) $(LIBRARY_3) $(TEST_OBJECTS)




#Used to remove everything from current directory except source files

.PHONY: clean
clean:
	rm -f --preserve-root \
		$(LIBRARY_3) $(LIB_OBJECTS) $(LIB_DEPENDS) \
		$(TEST_OBJECTS) $(TEST_DEPENDS)




#Used to install the library to the system's library directory

.PHONY: install
install: $(LIBRARY_3)
	cp -vf ./$(SRC_TOP_HEADER) $(HEADER_3_DEST)
	ln -vsf $(HEADER_3_DEST) $(HEADER_2_DEST)
	ln -vsf $(HEADER_3_DEST) $(HEADER_1_DEST)
	
	cp -vf ./$(LIBRARY_3) $(LIBRARY_3_DEST)
	ln -vsf $(LIBRARY_3_DEST) $(LIBRARY_2_DEST)
	ln -vsf $(LIBRARY_3_DEST) $(LIBRARY_1_DEST)




#Used to remove the library from the system

.PHONY: uninstall
uninstall:
	
	@#Remove the current version of SlyResult
	@#
	@#This is wrapped in $eval() blocks, because this MUST occur before
	@#the next lines, which are also wrapped in $eval() blocks.
	
	$(eval $(info $(shell rm -vf --preserve-root $(HEADER_3_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(HEADER_2_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(HEADER_1_DEST))))
	 
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_3_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_2_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_1_DEST))))
	
	
	@#See if old versions of SlyResult are available
	
	$(eval HEADER_1_MATCH= $(shell $(FIND_HEADER_1_MATCH)))	
	$(eval HEADER_2_MATCH= $(shell $(FIND_HEADER_2_MATCH)))
	
	$(eval LIBRARY_1_MATCH=$(shell $(FIND_LIBRARY_1_MATCH)))
	$(eval LIBRARY_2_MATCH=$(shell $(FIND_LIBRARY_2_MATCH)))
	
	
	@#If older versions of SlyResult are available, regenerate
	@#the symbolic links to point to them, to keep as many existing programs
	@#working as possible.
	@#
	@#Note how `ln` is _not_ passed the '-f' option
	
	if [[ ! -z "$(HEADER_1_MATCH)" ]]; \
	then \
		ln -vs $(HEADER_DIR)/$(HEADER_1_MATCH) $(HEADER_1_DEST); \
	fi
	
	if [[ ! -z "$(HEADER_2_MATCH)" ]]; \
	then \
		ln -vs $(HEADER_DIR)/$(HEADER_2_MATCH) $(HEADER_2_DEST); \
	fi
	
	if [[ ! -z "$(LIBRARY_1_MATCH)" ]]; \
	then \
		ln -vs $(LIBRARY_DIR)/$(LIBRARY_1_MATCH) $(LIBRARY_1_DEST); \
	fi
	
	if [[ ! -z "$(LIBRARY_2_MATCH)" ]]; \
	then \
		ln -vs $(LIBRARY_DIR)/$(LIBRARY_2_MATCH) $(LIBRARY_2_DEST); \
	fi




#Used to remove every version of SlyResult from the system

.PHONY: uninstall-all
uninstall-all:
	
	if [[ ! -z "$(PROJ_NAME)" ]]; \
	then \
		rm -vf --preserve-root $(HEADER_DIR)/$(PROJ_NAME)*.h; \
		rm -vf --preserve-root $(LIBRARY_DIR)/lib$(PROJ_NAME)*.so; \
	fi




#Build a list of dependencies between each source file at run-time. To do this
#requires parsing each source file with the compiler, but without actually
#compiling the files.
#
#Each source file gets its own dependency file, so that not every source file
#has to be reparsed when only a single source file changes.

$(LIB_DEPENDS) : .%.lib_dep : %
	rm -f --preserve-root ./$@
	$(CC) $(LIB_LDFLAGS) $(LIB_CFLAGS) -MM $^ > $@

include $(LIB_DEPENDS)


$(TEST_DEPENDS) : .%.test_dep : %
	rm -f --preserve-root ./$@
	$(CC) $(TEST_LDFLAGS) $(TEST_CFLAGS) -MM $^ > $@

include $(TEST_DEPENDS)




#Makes the main executable depend on all the other object files

$(LIB_OBJECTS):
	$(CC) $(LIB_LDFLAGS) $(LIB_CFLAGS) -c $(patsubst %.o, %.c, $@) -o $@


$(LIBRARY_3): $(LIB_OBJECTS)
	$(CC) $(LIB_LDFLAGS) $(LIB_OBJECTS) -shared -o $@




#Makes all of the test bench files compile as separate programs that link to
#SlyResult

$(TEST_OBJECTS): $(LIBRARY_3)
	$(CC) $(TEST_LDFLAGS) $(TEST_CFLAGS) $(patsubst %.o, %.c, $@) -o $@

