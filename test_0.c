/*!
 *  @file
 *  test_0.c
 *
 *  Very simple unit test that uses all of the functions declared inside of
 *  #SlyResult.h at least once.
 */




#include <stdio.h>




#include "./SlyResult.h"




/*!
 *  Prints out a variety of #SLY_STD_RES, #SlySr, and #SlyDr values
 */

int main ()
{
	for (int i = 0; i < 10; i += 1)
	{
		SLY_STD_RES res_0 =	(SLY_STD_RES) i;

		SlySr res_1 =		SlySr_get (res_0);

		SlyDr res_2 =		SlyDr_get (res_0);


		printf
		(
			"For i = %d, the #SLY_STD_RES is %s\n",
			i,
			SlyStdResStr_get (res_0).str
		);

		printf
		(
			"For i = %d, the #SlySr is %s\n",
			i,
			SlySrStr_get (res_1).str
		);
	
		printf
		(
			"For i = %d, the #SlyDr is %s\n",
			i,
			SlyDrStr_get (res_2).str
		);
		
		printf ("\n");
	}


	return 0;
}

